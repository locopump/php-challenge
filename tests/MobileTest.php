<?php

namespace Tests;

use App\Contact;
use App\Mobile;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class MobileTest extends TestCase
{

    /** @test */
    public function it_returns_null_when_name_empty()
    {
        $provider = m::mock('App\Interfaces\CarrierInterface');
        $provider->shouldReceive('makeCallByName')
            ->andReturn('');

        $mobile = new Mobile($provider);

        $this->assertNull($mobile->makeCallByName(''));
    }

    /** @test */
    public function it_returns_call_when_name_not_empty()
    {
        $provider = m::mock('App\Interfaces\CarrierInterface');
        $provider->shouldReceive('makeCallByName')
            ->andReturn();

        $mobile = new Mobile($provider);
        $this->assertTrue($mobile->makeCallByName('John'));
    }

}
