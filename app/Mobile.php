<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;


class Mobile
{

    protected $provider;

    function __construct(CarrierInterface $provider)
    {
        $this->provider = $provider;
    }


    public function makeCallByName($name = '')
    {
        if( empty($name) ) return;


        $contacts = array(
            ['name' => 'John', 'telephone' => '+51 999666555'],
            ['name' => 'Robert', 'telephone' => '+51 999444222'],
        );
        $service = new ContactService($contacts);

        $contact = $service->findByName($name);
//        print_r($contact);

//        $this->provider->dialContact($contact);
//        $this->provider->makeCall();
        return true;

//        return $this->provider->makeCall();
    }


}
