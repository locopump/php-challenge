<?php

namespace App\Services;

use App\Contact;


class ContactService
{
    protected $contacts;

    public function __construct(array $contacts)
    {
        $this->contacts = $contacts;
    }

    public function findByName($name = ''): Contact
	{

	    if (!empty($name)) {
	        $contact_name = '';
	        $contact_phone = '';

	        foreach ($this->contacts as $contacts) {
                if ($name == $contacts['name']) {
                    $contact_name = $name;
                    $contact_phone = $contacts['telephone'];
                }
            }

	        return new Contact($contact_name, $contact_phone);
        }
		return false;
	}

	public static function validateNumber(string $number): bool
	{
		// logic to validate numbers
	}
}