<?php

namespace App;


class Contact
{
    protected $name;
    protected $telephone;

	function __construct(string $name, string $telephone)
	{
		$this->name = $name;
		$this->telephone = $telephone;
	}
}